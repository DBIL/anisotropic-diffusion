%function imd = PCTCoherenceEnhancingDiffusion3D(im,k,s,n,isplot)
% The diffusion tensor has the form:
%           |imc1  0|
%   D = R^T |       | R
%           |0  imc2|
%
% R - rotation matrix whose columns are eigenvectors of the structure
% tensor S(indicating the local orientation of an imahe patch) 
% ==== S could be the PCT ======
% 
%       |s11 s12|
%   S = |       |
%       |s21 s22|
%
% c1 and c2 are the conductivity coefficients along principle directions 
%% Path - Libs
im = imread('lines_bb.png');
im = double(im); im = (im - min(im(:))) / (max(im(:)) - min(im(:)));
idxT = find(im<0.95);
idxB = find(im>=0.95);
%im = imread('lines_bbn.png');
%im = imread('../../2010/BOCostMaps/images/testLines1n2.png');
%im = imnoise(im,'gaussian',0.0,0.03);
%load('../../2010/BOCostMaps/mat/netSmall.mat');
%load('../../2010/BOVectorFieldIntersections2D/mat/im0255phvess.mat');
%%
% center = [1,1]; %[0,0] is the middle of the image, [pi,pi] is the lower right
% orientation = pi/4; %radians (pi/4 = 45 degrees)
% [xn,yn] = size(im);
% [X,Y] = meshgrid(linspace(-pi,pi,xn),linspace(-pi,pi,yn));
% imr = cos(orientation)*(X-center(1)) + sin(orientation)*(Y-center(2));
% imr = double(imr); imr = (imr - min(imr(:))) / (max(imr(:)) - min(imr(:)));
% % let's take a quick look at ramp
% % figure(1)
% % imagesc(imr); colormap(gray(256));
% % axis off; axis equal;
% imr = imcomplement(imr);
% imn = imnoise(zeros(size(im)),'gaussian',0.0,0.01);
% imn = imn.*imr;
% imn = double(imn); imn = (imn - min(imn(:))) / (max(imn(:)) - min(imn(:)));
% % figure;
% % imagesc(1-imn); colormap(gray(256));
% % axis off; axis equal;
% im = double(im); 
% im = (im - min(im(:))) / (max(im(:)) - min(im(:)));
% im = im + (.5*(1-imn));
% im = (im - min(im(:))) / (max(im(:)) - min(im(:)));

% figure;
% imagesc(im); colormap(gray(256));
% axis off; axis equal;
%return
%% Normalize
im = double(im); im = (im - min(im(:))) / (max(im(:)) - min(im(:)));
%im = imnoise(im,'gaussian',0.0,0.01);
%im = double(im); im = (im - min(im(:))) / (max(im(:)) - min(im(:)));
sT = mean(im(idxT));
sB = mean(im(idxB));
sigmaT = std(im(idxT));
sigmaB = std(im(idxB));
CNR1 = (2*(sT-sB)^2)/(sigmaT^2+sigmaB^2);
CNR0 = (2*(sT)^2)/(sigmaT^2);
% figure; imagesc(im); colormap gray;
% set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
% return
%im = im2double(im);
%im = im( 51:125,76:150 );
%im = imcrop(im,[14.5100   45.5100  247.9800  218.9800]);
%im = imcrop(im,[18.5100  345.5100  239.9800  216.9800]);
k = 0.9; o = 0.7; t = 3; s = 0.2; n = 60; isplot = 1; type = 1;
%% Plot
if isplot==1
    figure;%(isplot); 
    subplot(1,2,1); imshow(im,[]); title('Image'); drawnow;
end
%% Run
imd = im;
for i=1:n
    %i
    %% Tensor
    %sigma = 2:1:4; gamma = 2; beta = 0.5; c = []; wb = true;
    %[V,Vmax,Vidx,Vx,Vy,L1,L2] = BOVesselnessV2D(imcomplement(imd),sigma,gamma,beta,c,wb);
    %[V,Vmax,Vidx,Vx,Vy,L1,L2] = BOVesselnessV2D(imd,sigma,gamma,beta,c,wb);
    
    beta = 0.5; c = 15;
    %beta = 2.8; c = 15;
    %nscale = 5; norient = 48; minWaveLength = 10; mult = 1.5; sigmaOnf = 0.5;
    %k = 10.0; cutOff = 0.4; g = 10; noiseMethod = -2;
    % GRID
    %nscale = 4; norient = 6; minWaveLength = 4; mult = 2.5; sigmaOnf = 0.5; 
    %kk = 2.0; cutOff = 0.4; g = 10; noiseMethod = -2;
    % GRID noise
    %beta = 1.5; c = .5;
    %nscale = 5; norient = 48; minWaveLength = 10; mult = 1.5; sigmaOnf = 0.5;
    %kk = 10.0; cutOff = 0.4; g = 10; noiseMethod = -2;
%     beta = 1.5; c = .5;
%     nscale = 5; norient = 6; minWaveLength = 10; mult = 1.5; sigmaOnf = 0.5;
%     kk = 10.0; cutOff = 0.4; g = 10; noiseMethod = -2;
    % NETWORK
    nscale = 5; norient = 6; minWaveLength = 3; mult = 1.5; sigmaOnf = 0.6;
    kk = 2.0; cutOff = 0.4; g = 10; noiseMethod = -2;
    % RETINA
    %nscale = 3; norient = 12; minWaveLength = 6; mult = 1.8; sigmaOnf = 0.6;
    %kk = 2.0; cutOff = 0.6; g = 10; noiseMethod = -1;
    %beta = 10.5; c = 0.5;
    % nscale      = 5;    %6    %3
    % norient     = 6;    %10   %6
    % minWaveLength = 8;  %2.5  %3 
    % mult        = 2.0;  %1.8  %2.0
    % sigmaOnf    = 1.2; %1.4  %1.2
    % k           = 2.0;  %2.0  %2.0
    % cutOff      = 0.4;  %0.4  %0.4
    % g           = 10;   %10   %10
    %noiseMethod = -1;   %-1   %-1
    %[Vmax,Vx,Vy,L1,L2] = BOPhaseVesselnessFilter(beta,c,im,nscale,norient,minWaveLength,mult,sigmaOnf,kk,cutOff,g,noiseMethod);
    [M,m,or,featType,PC,EO,T,pcSum] = phasecong3(im,nscale,norient,minWaveLength,mult,sigmaOnf,kk,cutOff,g,noiseMethod);
    %[Vmax,Vx,Vy,L1,L2] = BOPhaseVesselnessFilter(beta,c,imd,nscale,norient,minWaveLength,mult,sigmaOnf);
    %sT = mean(Vmax(idxT));
    %sB = mean(Vmax(idxB));
    %sigmaT = std(Vmax(idxT));
    %sigmaB = std(Vmax(idxB));
    %CNR2 = (2*(sT-sB)^2)/(sigmaT^2+sigmaB^2);
    %figure; imagesc(Vmax); colormap gray;
    %set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
    %return
    %load('./mat/retina_mask.mat');
    %Vmax(~immask) = 0;
    %Vx(~immask) = 0;
    %Vy(~immask) = 0;
    %figure; imagesc(Vmax); colormap gray;
    %set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
    %return
    %% Structure Tensor
    [Vx,Vy] = BORotateVector2D(Vx,Vy,0,0,pi/2);    
    % Rotate 90
    imgx = Vmax.*(-Vy);
    imgy = Vmax.*( Vx);
    ims11 = BOGaussianDerivative2D(imgx.^2,t,0,0); 
    ims12 = BOGaussianDerivative2D(imgx.*imgy,t,0,0);
    ims22 = BOGaussianDerivative2D(imgy.^2,t,0,0);
    %imgx = Vx;
    %imgy = Vy;
    %ims11 = BOGaussianDerivative2D(Vmax.*imgx.^2,t,0,0); 
    %ims12 = BOGaussianDerivative2D(Vmax.*imgx.*imgy,t,0,0);
    %ims22 = BOGaussianDerivative2D(Vmax.*imgy.^2,t,0,0);
    %ims11 = Vmax.*(Vx.^2); 
    %ims12 = Vmax.*(Vx.*Vy);
    %ims22 = Vmax.*(Vy.^2);
    %% Eigendecomposition
    % Calculating the determinant, this yields the quadratic equation 
    % whose solutions (also called roots) 
    % eigenvalues e1,e2    
    imel1 = ((ims11+ims22)-sqrt((ims11-ims22).^2+4*ims12.^2))/2;
    imel2 = ((ims11+ims22)+sqrt((ims11-ims22).^2+4*ims12.^2))/2;    
    % eigenvectors v1,v2
    imv1 = ims12./(sqrt(ims12.^2+(imel1-ims11).^2)+eps); 
    imv2 = (imel1-ims11)./(sqrt(ims12.^2+(imel1-ims11).^2)+eps);    
    %% C1 and C2 - diffusion 'speed'   
    %ime = (imel1-imel2).^2;
    %ime = (L1-L2).^2;
    ime = Vmax;
    h = fspecial('gaussian',3*1,1);
    ime = imfilter(ime,h,'replicate');
    m2 = max(ime(:)); m1 = min(ime(:));
    r = m2-m1; k2 = m1 + (1-k)*r;
    
    imc1 = max(0.01,1-exp(-ime/k2));
    imc2 = 0.01;    
    %imc1 = max(0.01,1-exp(-(imel1-imel2).^2/k^2)); %OK
    %imc2 = 0.01; %OK
    %imc1 = Vmax;
    %imc2 = 0.01;
    %% Diffusion Tensor Components
    imd11 = imc1.*imv1.^2 + imc2.*imv2.^2;
    imd12 = (imc1-imc2).*imv1.*imv2;
    imd22 = imc1.*imv2.^2 + imc2.*imv1.^2;
    %% Steps
    imd = imd + s*BOTensorNonLinearDiffusion2D(imd,imd11,imd12,imd22,type);
    %% Plot
    if isplot==1
        %figure(isplot); 
        %subplot(1,2,2); imshow(imd,[]); title(['Diffusion - nr. ' num2str(i)]); drawnow;
        subplot(1,2,2); 
        imfc = imd-im; imfc(imfc>0)=0;
        imshow(imfc,[]);
        set(gca,'ytick',[]); set(gca,'xtick',[]); axis equal; axis tight;
        title(['Diffusion - nr. ' num2str(i)]); drawnow;
        pause(0.02);
    end
end
%% CNR
sT = mean(imd(idxT));
sB = mean(imd(idxB));
sigmaT = std(imd(idxT));
sigmaB = std(imd(idxB));
CNR2 = (2*(sT-sB)^2)/(sigmaT^2+sigmaB^2);
CNR3 = (2*(sT)^2)/(sigmaT^2);
return
%rect = [35.5100   35.5100  230.9800  229.9800];
rect = [1 1 size(im,2) size(im,1)];
imc = imcrop(im,rect);
figure; imagesc(imc); colormap gray;
set(gca,'ytick',[]); set(gca,'xtick',[]); axis equal; axis tight;
print('-dpng','-r300','in.png');
imdc = imcrop(imd,rect);
figure; imagesc(imdc); colormap gray;
set(gca,'ytick',[]); set(gca,'xtick',[]); axis equal; axis tight;
print('-dpng','-r300','adf_pct.png');
%imfc = imcrop(abs(imd-im),rect);
imfc = imcrop((imd-im)./im,rect);
imfc(imfc>0)=0;
figure; imagesc(imfc); colormap gray;
set(gca,'ytick',[]); set(gca,'xtick',[]); axis equal; axis tight;
print('-dpng','-r300','diff_pct.png');
%figure; plot(1-imc(:,16),'r-o');

return
imf = imfill(imc,'holes');
figure; imagesc(imf); colormap gray;
set(gca,'ytick',[]); set(gca,'xtick',[]); axis equal; axis tight;
figure; plot(1-imf(:,16),'r-o');

x1 = 30; x2 = 40;
figure; plot(1-imc(x1,:),'r-o'); hold on; plot(1-imc(x2,:),'b-^');
figure; plot(1-imdc(x1,:),'r-o'); hold on; plot(1-imdc(x2,:),'b-^');
figure; plot(imfc(x1,:),'r-o'); hold on; plot(imfc(x2,:),'b-^');
x1 = 80; x2 = 90;
figure; plot(1-imc(x1,:),'r-o'); hold on; plot(1-imc(x2,:),'b-^');
figure; plot(1-imdc(x1,:),'r-o'); hold on; plot(1-imdc(x2,:),'b-^');
figure; plot(imfc(x1,:),'r-o'); hold on; plot(imfc(x2,:),'b-^');
x1 = 130; x2 = 140;
figure; plot(1-imc(x1,:),'r-o'); hold on; plot(1-imc(x2,:),'b-^');
figure; plot(1-imdc(x1,:),'r-o'); hold on; plot(1-imdc(x2,:),'b-^');
figure; plot(imfc(x1,:),'r-o'); hold on; plot(imfc(x2,:),'b-^');
%% End
end