%% Clear
clc; clear all; close all;
%% Load Image
im = imread('1imcp.png');
im = im2double(im);
%% PDF
k = 0.001; o = 0.7; t = 5; s = 0.2; n = 10; isplot = 1;
imd = PCTCoherenceEnhancingDiffusion3D(im,k,s,n,isplot);
return
%% Plot
figure;
[X,Y] = meshgrid(1:size(im,2),1:size(im,1));
imagesc(im); hold on; quiver(X,Y,imv2,imv1,'.');
%imagesc(im); hold on; quiver(X,Y,Vy,Vx);
%imagesc(im); hold on; quiver(X,Y,imgy,imgx);
colormap gray; set(gca,'ytick',[]); set(gca,'xtick',[]); axis equal; axis tight;
